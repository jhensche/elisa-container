echo "Substitute secrets"
sed -i "s/secret1/${secret1}/g" config/application-${cfg_profile}.properties
sed -i "s/secret2/${secret2}/g" config/application-${cfg_profile}.properties
sed -i "s/sso_clientID/${sso_clientID}/g" /etc/httpd/conf.d/httpd-elisa.conf
sed -i "s/sso_clientSecret/${sso_clientSecret}/g" /etc/httpd/conf.d/httpd-elisa.conf
sed -i "s,sso_issuerURL,${sso_issuerURL},g" /etc/httpd/conf.d/httpd-elisa.conf
sed -i "s,sso_redirectURL,${sso_redirectURL},g" /etc/httpd/conf.d/httpd-elisa.conf
sed -i "s/sso_suggestedCookieSecret/${sso_suggestedCookieSecret}/g" /etc/httpd/conf.d/httpd-elisa.conf
sed -i "s/cfg_profile/${cfg_profile}/g" elisa.conf
sed -i "s/cfg_logbookName/${cfg_logbookName}/g" /etc/httpd/conf.d/httpd-elisa.conf
sed -i "s/cfg_cernRoles/${cfg_cernRoles}/g" /etc/httpd/conf.d/httpd-elisa.conf

echo "Start httpd"
httpd

echo "Execute elisa.jar"
./elisa.jar
