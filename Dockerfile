FROM gitlab-registry.cern.ch/linuxsupport/alma9-base

EXPOSE 8080 8443

RUN dnf install -y 'dnf-command(config-manager)' && \
    dnf config-manager --add-repo https://atlas-tdaq-sw.web.cern.ch/atlas-tdaq-sw/yum/tdaq/elisa/el9 && \
    dnf install -y java-11-openjdk elisa httpd mod_auth_openidc --nogpgcheck && \
    dnf erase -y 'dnf-command(config-manager)' && dnf clean all

WORKDIR /var/local/elisa
RUN mkdir -p config

COPY config config
COPY elisa.conf .
COPY entrypoint.sh .

COPY httpd-elisa.conf /etc/httpd/conf.d
RUN sed -i "s/Listen 80/Listen 8080/g" /etc/httpd/conf/httpd.conf


RUN chgrp -R 0 /var/local/elisa && \
    chmod -R g=u /var/local/elisa

RUN chgrp -R 0 /run && chmod -R g=u /run
RUN chgrp -R 0 /etc/httpd && chmod -R g=u /etc/httpd
RUN chgrp -R 0 /etc/httpd/logs && chmod -R g=u /etc/httpd/logs

RUN chmod 775 entrypoint.sh

USER 1001

ENTRYPOINT ./entrypoint.sh
